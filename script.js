function createNewUser() {
  let name = prompt("Ваше ім'я?");
  let lastName = prompt("Ваша фамілія?");
  let birthday = prompt(
    "Введіть дату народження у форматі dd.mm.yyyy",
    "01.01.2000"
  );

  // Розділити рядок дати на день, місяць і рік
  const dateParts = birthday.split(".");

  // Перетворити рядок у числа
  const day = parseInt(dateParts[0], 10);
  const month = parseInt(dateParts[1] - 1, 10); // Місяці в JavaScript ідуть від 0 до 11
  const year = parseInt(dateParts[2], 10);

  const birthDate = new Date(year, month, day);

  const newUser = {
    get name() {
      return name;
    },
    set name(newName) {
      name = newName;
    },

    get lastName() {
      return lastName;
    },
    set lastName(newLastName) {
      lastName = newLastName;
    },

    get birthday() {
      return birthday;
    },

    getAge: function calculateAge() {
      const today = new Date();

      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDiff = today.getMonth() - birthDate.getMonth();

      if (
        monthDiff < 0 ||
        (monthDiff === 0 && today.getDate() < birthDate.getDate())
      ) {
        age--;
      }

      return age;
    },

    getPassword: function () {
      return this.name[0].toUpperCase() + this.lastName.toLowerCase();
    },
  };

  const login = newUser.getPassword();
  console.log(`Логін: ${login}`);
  const age = newUser.getAge();
  console.log(`Вік: ${age}`);
}

createNewUser();
